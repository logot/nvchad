local keymap = require "custom.configs.keymap"
local cmd, set = keymap.cmd, keymap.set

return {
  set("n", "<leader>zz", cmd "zig run", { buffer = true }),
  set("n", "<leader>zb", cmd "compiler zig_build", { buffer = true }),
  set("n", "<leader>zt", cmd "compiler zig_test", { buffer = true }),
}
