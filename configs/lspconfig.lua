local on_attach = require("plugins.configs.lspconfig").on_attach
local capabilities = require("plugins.configs.lspconfig").capabilities
local lspconfig = require "lspconfig"
-- if you just want default config for the servers then put them in a table
local servers = { "html", "cssls", "tsserver", "clangd", "lua_ls" }

lspconfig.rust_analyzer.setup {
  on_attach = on_attach,
  capabilities = capabilities,
}

local elixir_path = vim.env.HOME .. "/.local/share/nvim/mason/packages/elixir-ls/language_server.sh"
lspconfig.elixirls.setup {
  cmd = { elixir_path },
  on_attach = on_attach,
  capabilities = capabilities,
}

lspconfig.zls.setup {
  on_attach = function(client, bufnr)
    require("custom.configs.zig-tools")
    on_attach(client, bufnr)
  end,
  capabilities = capabilities,
}

for _, lsp in ipairs(servers) do
  lspconfig[lsp].setup {
    on_attach = on_attach,
    capabilities = capabilities,
  }
end

--
-- lspconfig.pyright.setup { blabla}
