local keymap = {}

function keymap.cmd(str)
  return "<cmd>" .. str .. "<CR>"
end

function keymap.set(mod, key, run, opts)
  return vim.keymap.set(mod, key, run, opts)
end

return keymap
