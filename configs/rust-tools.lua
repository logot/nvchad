local mason_path = vim.env.HOME .. "/.local/share/nvim/mason/"
local codelldb_path = mason_path .. "packages/codelldb/extension/adapter/codelldb"
local liblldb_path = mason_path .. "packages/codelldb/extension/lldb/lib/liblldb.so"

local keymap = require "custom.configs.keymap"
local cmd, set = keymap.cmd, keymap.set

local options = {
  tools = { -- rust-tools options
    inlay_hints = {
      -- prefix for parameter hints
      -- default: "<-"
      parameter_hints_prefix = "<- ",
      -- prefix for all the other hints (type, chaining)
      -- default: "=>"
      other_hints_prefix = "=> ",
      -- The color of the hints
      highlight = "Comment",
    },
    crate_graph = {
      backend = "jpg"
    }
  },
  server = {
    on_attach = function(_, bufnr)
      set("n", "<leader>mt", cmd "RustTest", { buffer = bufnr })
      set("n", "<leader>mh", cmd "RustHoverActions", { buffer = bufnr })
      set({ "n", "v" }, "<leader>a", cmd "RustCodeActions", { buffer = bufnr })
      set("n", "<leader>md", cmd "RustDebuggables", { buffer = bufnr })
      set("n", "<leader>mr", cmd "RustRunnables", { buffer = bufnr })
      set("n", "<leader>rr", cmd "cargo run", { buffer = bufnr })
      set("n", "<leader>me", cmd "RustExpandMacro", { buffer = bufnr })
      set("n", "<leader>fp", cmd "RustOpenCargo", { buffer = bufnr })
      set("n", "<leader>mp", cmd "RustParentModule", { buffer = bufnr })
      set("n", "<leader>ms", cmd "RustSSR", { buffer = bufnr })
      set("n", "<leader>mg", cmd "RustViewCrateGraph", { buffer = bufnr })
    end,
    -- to enable rust-analyzer settings visit:
    -- https://github.com/rust-analyzer/rust-analyzer/blob/master/docs/user/generated_config.adoc
    settings = {
      ["rust-analyzer"] = {
        checkOnSave = {
          enable = true,
        },
        inlayHints = {
          lifetimeElisionHints = {
            enable = true,
            useParameterNames = true,
          },
        },
      },
    },
  },
  dap = {
    dapter = require("rust-tools.dap").get_codelldb_adapter(codelldb_path, liblldb_path),
  },
}

return options
