local options = {
  load = {
    ["core.defaults"] = {}, -- Loads default behaviour
    ["core.concealer"] = {}, -- Adds pretty icons to your documents
    ["core.dirman"] = { -- Manages Neorg workspaces
      config = {
        workspaces = {
          notes = "~/devs/notes",
          home = "~/devs/notes/home",
          work = "~/devs/notes/work",
        },
        default_workspace = "notes",
      },
    },
  },
}

return options
