---@type MappingsTable
local M = {}
local cmd = require("custom.configs.keymap").cmd

M.general = {
  n = {
    [";"] = { ":", "enter command mode", opts = { nowait = true } },
  },
}

M.lazy = {
  n = {
    ["<leader>ls"] = { cmd "Lazy sync", "Lazy sync" },
    ["<leader>li"] = { cmd "Lazy install", "Lazy install" },
    ["<leader>lu"] = { cmd "Lazy update", "Lazy update" },
    ["<leader>lc"] = { cmd "Lazy clean", "Lazy clean" },
  },
}

-- more keybinds!
M.nvterm = {
  plugin = true,

  t = {
    -- toggle in terminal mode
    ["ƒ"] = {
      function()
        require("nvterm.terminal").toggle "float"
      end,
      "Toggle floating term",
    },
    ["ß"] = {
      function()
        require("nvterm.terminal").toggle "horizontal"
      end,
      "Toggle horizontal term",
    },

    ["√"] = {
      function()
        require("nvterm.terminal").toggle "vertical"
      end,
      "Toggle vertical term",
    },
  },

  n = {
    -- toggle in normal mode
    ["ƒ"] = {
      function()
        require("nvterm.terminal").toggle "float"
      end,
      "Toggle floating term",
    },

    ["ß"] = {
      function()
        require("nvterm.terminal").toggle "horizontal"
      end,
      "Toggle horizontal term",
    },

    ["√"] = {
      function()
        require("nvterm.terminal").toggle "vertical"
      end,
      "Toggle vertical term",
    },
  },
}

M.dap = {
  n = {
    ["<leader>db"] = { cmd "DapToggleBreakpoint", "Toggle breakpoint" },
    ["<leader>dso"] = { cmd "DapStepOver", "Step Over" },
    ["<leader>dsi"] = { cmd "DapStepInto", "Step Into" },
    ["<leader>dst"] = { cmd "DapStepOut", "Step Out" },
    ["<leader>dc"] = { cmd "DapContinue", "Continue" },
    ["<leader>dt"] = { cmd "DapTerminate", "Terminate" },
  },
}

M.dapui = {
  n = {
    ["<leader>dd"] = {
      function()
        require("dapui").toggle()
      end,
      "Toggle debugging ui ",
    },
  },
}

M.crates = {
  n = {
    ["<leader>rcu"] = {
      function()
        require("crates").upgrade_all_crates()
      end,
      "Update crates",
    },
  },
}

M.trouble = {
  n = {
    ["<leader>tt"] = { cmd "TroubleToggle", "Touble Toggle" },
    ["<leader>tn"] = {
      function()
        require("trouble").next { skip_groups = true, jump = true }
      end,
      "Trouble Next",
    },
    ["<leader>tp"] = {
      function()
        require("trouble").previous { skip_groups = true, jump = true }
      end,
      "Trouble Previous",
    },
    ["<leader>tf"] = {
      function()
        require("trouble").first { skip_groups = true, jump = true }
      end,
      "Trouble First",
    },
    ["<leader>tl"] = {
      function()
        require("trouble").last { skip_groups = true, jump = true }
      end,
      "Trouble Last",
    },
  },
}

return M
