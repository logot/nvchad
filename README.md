# Introduce

This is a custom config for NvChad. Do check the feature_full branch if you need all the ease in your config.

## Release
### 0.0.1
- Apply overrides for nvim-cmp
- Add Rust, Elixir, Zig language environment
  + Language LSP mapping refactoring
- Add folke's plugins (noice, trouble)
- Add *Neorg* (notetaking)
